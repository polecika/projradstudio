//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Media.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <System.IOUtils.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLabel *laPath;
	TListView *lv;
	TLayout *lyTop;
	TLayout *lyBottom;
	TLayout *lyCurrentTime;
	TMediaPlayer *mp;
	TTimer *tm;
	TTrackBar *tbCurrentTime;
	TLabel *laTitle;
	TButton *buAbout;
	TButton *buPlay;
	TButton *buPause;
	TButton *buStop;
	TButton *buPrev;
	TButton *buNext;
	TLabel *Label2;
	TLabel *laDuration;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall lvItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall buPauseClick(TObject *Sender);
	void __fastcall buStopClick(TObject *Sender);
	void __fastcall tbCurrentTimeChange(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
	int FPauseTime;
	void DoRefreshList(UnicodeString aPath);
	void DoStop();
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
