//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <FMX.Edit.hpp>
#include <string>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TLabel *Label1;
	TMemo *Memo1;
	TButton *Button1;
	TScrollBar *SB;
	TLabel *Label4;
	TEdit *Edit1;
	TGridPanelLayout *GridPanelLayout1;
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
void siftDown(int *numbers, int root, int bottom);
void heapSort(int *numbers, int array_size);
//System::UnicodeToUtf8() *array;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------

#endif
