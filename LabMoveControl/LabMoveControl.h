//---------------------------------------------------------------------------

#ifndef LabMoveControlH
#define LabMoveControlH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TCircle *Circle1;
	TRectangle *Rectangle1;
	TRoundRect *RoundRect1;
	TImage *Image1;
	void __fastcall MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall MouseEnter(TObject *Sender);
	void __fastcall MouseLeave(TObject *Sender);
	void __fastcall MouseMove(TObject *Sender, TShiftState Shift, float X, float Y);
	void __fastcall MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);



private:	// User declarations
	float FX, FY;
	bool FIsDragging;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
