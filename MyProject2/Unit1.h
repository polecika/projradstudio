//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TGridPanelLayout *GridPanelLayout1;
	TImage *Image1;
	TImage *Image2;
	TImage *Image3;
	TPanel *Panel1;
	TButton *Button1;
	TButton *Button2;
	TGridPanelLayout *GridPanelLayout2;
	TLabel *Label1;
	TLabel *LabelBotScore;
	TLabel *LabelPlayerScore;
	TLabel *Finish;
	TImage *Image4;
	TImage *Image5;
	TImage *Image6;
	TStyleBook *StyleBook1;
	void __fastcall ImageClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);

private:	// User declarations
    int PlayerScore, BotScore;
public:		// User declarations
	int Player;
	__fastcall Tfm(TComponent* Owner);
    void Game(int a);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
