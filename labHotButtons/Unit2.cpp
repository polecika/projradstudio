//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ButtonAllMouseEnter(TObject *Sender)
{
	TButton *x = ((TButton*)Sender);
	x->Margins->Rect = TRect(0,0,0,0);
	x->TextSettings->Font->Size += 10;
	x->TextSettings->Font->Style = x->TextSettings->Font->Style << TFontStyle::fsBold;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ButtonAllMouseLeave(TObject *Sender)
{
    TButton *x = ((TButton*)Sender);
	x->Margins->Rect = TRect(5,5,5,5);
	x->TextSettings->Font->Size -= 10;
	x->TextSettings->Font->Style = x->TextSettings->Font->Style >> TFontStyle::fsBold;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall Tfm::Panel1MouseEnter(TObject *Sender)
{
	//TPanel *x = ((TPanel*)Sender);
	//x->Position->X = 0;
}
//---------------------------------------------------------------------------
