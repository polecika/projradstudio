//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buPasswordClick(TObject *Sender)
{
	edPassword ->Text = RandomStr(StrToIntDef(edLength->Text,9),
	ckLower -> IsChecked, ckUpper ->IsChecked, ckNumber ->IsChecked,
	ckSpek -> IsChecked);
}
//---------------------------------------------------------------------------
