//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
inline int Low(const System::UnicodeString &S)
{
#ifdef _DELPHI_STRING_ONE_BASED
	return 1;
#else
	return 0;
#endif
}
//---------------------------------------------------------------------------
inline int High(const System::UnicodeString &S)
{
#ifdef _DELPHI_STRING_ONE_BASED
	return S.Length();
#else
	return S.Length() - 1;
#endif
}
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	System::UnicodeString x;
	//  ����� �������
	for (int i = 0; i < mefull->Lines->Count; i++) {
		x = mefull->Lines->Strings[i];
		if (i % 2 == 1) {
			for (int j = Low(x); j <= High(x); j++) {
				if (x[j] != ' ')
					x[j] = 'x';
			}

		}
		me1->Lines->Add(x);

	}
	//������ �����
	bool xFlag;
	for (int i = 0; i < mefull->Lines->Count; i++) {
		x = mefull->Lines->Strings[i];
		xFlag = false;
		for (int j = Low(x); j <= High(x); j++) {
			if ((xFlag) && (x[j] != ' '))   {
				x[j] = 'x';
			}
			if ((!xFlag) && (x[j] == ' '))  {
				xFlag = true;
			}
		}
		me2->Lines->Add(x);
	}

}
//---------------------------------------------------------------------------
