//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Twb *wb;
//---------------------------------------------------------------------------
__fastcall Twb::Twb(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Twb::Button1Click(TObject *Sender)
{
 wb->GoBack();
}
//---------------------------------------------------------------------------
void __fastcall Twb::Button2Click(TObject *Sender)
{
 wb->GoForward();
}
//---------------------------------------------------------------------------
void __fastcall Twb::Button3Click(TObject *Sender)
{
    wb->Reload();
}
//---------------------------------------------------------------------------
void __fastcall Twb::Button4Click(TObject *Sender)
{
    ShowMessage("Hello, world!");
}
//---------------------------------------------------------------------------
void __fastcall Twb::GOClick(TObject *Sender)
{
	wb->URL=edURL->Text;
}
//---------------------------------------------------------------------------
void __fastcall Twb::wbDidFailLoadWithError(TObject *ASender)
{
	edURL->Text=wb->URL;
}

void __fastcall Twb::edURLKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if(Key == vkReturn){
	//��� ����� enter
	wb->URL = edURL->Text;
	}
}
//---------------------------------------------------------------------------

