//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.WebBrowser.hpp>
//---------------------------------------------------------------------------
class Twb : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TToolBar *ToolBar2;
	TWebBrowser *wb;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TEdit *edURL;
	TButton *GO;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall GOClick(TObject *Sender);
   	void __fastcall wbDidFailLoadWithError(TObject *ASender);
	void __fastcall edURLKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	//void __fastcall ToolBar2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Twb(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Twb *wb;
//---------------------------------------------------------------------------
#endif
