//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit3.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm3 *Form3;
//---------------------------------------------------------------------------
__fastcall TForm3::TForm3(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm3::buStartClick(TObject *Sender)
{
	TJSONObject *xObj;
	TJSONArray *xArr;
	//
	xObj = (TJSONObject*)TJSONObject::ParseJSONValue(meJSON->Text);
	//

	xObj = (TJSONObject*)TJSONObject::ParseJSONValue(xObj->GetValue("response")->ToString());
	meLogs->Lines->Add("count: " + xObj->GetValue("count")->Value());
	//
	xArr = (TJSONArray*)TJSONObject::ParseJSONValue(xObj->GetValue("items")->ToString());
	//
	xObj = (TJSONObject*)TJSONObject::ParseJSONValue(xArr->Items[0]->ToString());
	meLogs->Lines->Add("id: " + xObj->GetValue("id")->Value());
	meLogs->Lines->Add("title: " + xObj->GetValue("id")->Value());
	//
	xArr->DisposeOf();
	xObj->DisposeOf();
}
//---------------------------------------------------------------------------
