//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Game.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void Tfm::DoReset()
{
	FCountCorrect = 0;
	FCountWrong = 0;
	FTimeValue = Time().Val + (float)30/(24*60*60); //30 sec
	tmPlay->Enabled = true;
    DoContinue();
}
void Tfm::DoContinue()
{
	 const UnicodeString c1 = "02468";  //������
	 const UnicodeString c2 = "13579"; //��������
	 const UnicodeString c3 = L"���������"; //������� �����
	 const UnicodeString c4 = L"������������������"; //��������� �����
	 //
	 laCorrect->Text = Format(L"���� %d", ARRAYOFCONST((FCountCorrect)));
	 //
	 bool xEven = (Random(2) == 1);
	 bool xVowel = (Random(2) == 1);
	 bool xBoxFirst = (Random(2) == 1);
	 System::UnicodeString xResult ="";
	 //
	 if (xEven) {
		 xResult += c1.SubString0(Random(c1.Length()), 1);
	 }
	 else
		xResult += c2.SubString0(Random(c2.Length()), 1);
	 //
	 if (xVowel) {
		 xResult += c3.SubString0(Random(c3.Length()), 1);
	 }
	 else
	 xResult += c4.SubString0(Random(c4.Length()), 1);
	 //
	 if (xBoxFirst) {
		 FAnswerCorrect = xEven;
		 laBox1->Text = xResult;
		 laBox2->Text = "";
	 }
	 else {
		  FAnswerCorrect = xVowel;
		 laBox1->Text = "";
		 laBox2->Text = xResult;
	 }
}
void Tfm::DoAnswer(bool aValue)
{
	(aValue == FAnswerCorrect) ? FCountCorrect++ : FCountWrong++;
	DoContinue();

}
void Tfm::DoFinish()
{
	tmPlay -> Enabled = false;
	laFinishCorrect->Text = Format(L"���������� : %d",
	ARRAYOFCONST((FCountCorrect)));
	laFinishWrong->Text = Format(L"He���������� : %d",
	ARRAYOFCONST((FCountWrong)));
	tc->ActiveTab = tiFinish;


}
void __fastcall Tfm::tmPlayTimer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	laTime->Text = FormatDateTime("nn:ss", x);
	if (x <= 0) {
		DoFinish();
	}
}
//---------------------------------------------------------------------------



void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc ->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::Button1Click(TObject *Sender)
{
 tc ->ActiveTab = tiPlay;
 DoReset();
}
//---------------------------------------------------------------------------


void __fastcall Tfm::Button7Click(TObject *Sender)
{
 tc->ActiveTab = tiPlay;
 DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button8Click(TObject *Sender)
{
 tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button9Click(TObject *Sender)
{
 tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button2Click(TObject *Sender)
{
    tc ->ActiveTab = tiHelp;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::Button6Click(TObject *Sender)
{
 tc ->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::Button4Click(TObject *Sender)
{
    DoAnswer(true);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button5Click(TObject *Sender)
{
	DoAnswer(false);
}
//---------------------------------------------------------------------------


