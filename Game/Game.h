//---------------------------------------------------------------------------

#ifndef GameH
#define GameH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiPlay;
	TTabItem *tiFinish;
	TTabItem *tiHelp;
	TLabel *Label2;
	TLayout *Layout1;
	TTabItem *tiMenu;
	TLabel *Label1;
	TGridPanelLayout *GridPanelLayout2;
	TButton *Button4;
	TButton *Button5;
	TLayout *Layout2;
	TLabel *Label3;
	TLayout *Layout3;
	TGridPanelLayout *GridPanelLayout3;
	TLabel *Label4;
	TRectangle *Rectangle1;
	TLabel *Label5;
	TRectangle *Rectangle2;
	TLabel *laBox1;
	TLabel *laBox2;
	TButton *Button6;
	TLabel *laTime;
	TLabel *laCorrect;
	TLayout *Layout4;
	TLabel *Label10;
	TLabel *laFinishCorrect;
	TLabel *laFinishWrong;
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TGridPanelLayout *GridPanelLayout4;
	TButton *Button7;
	TButton *Button8;
	TMemo *Memo1;
	TLayout *Layout5;
	TButton *Button9;
	TTimer *tmPlay;
	TLabel *Label13;
	void __fastcall tmPlayTimer(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
	 int FCountCorrect;
	 int FCountWrong;
	 bool FAnswerCorrect;
	 double FTimeValue;
	 void DoReset();
	 void DoContinue();
	 void DoFinish();
	 void DoAnswer(bool aValue);

};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
