//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
 const UnicodeString cList[] = {
	 "coupe", "hardtop", "hatchback", "liftback", "limousine", "minibus","minivan",
	 "sedan", "universal"
 };

 void Tfm::LoadItem(int aIndex)
 {
	 UnicodeString xName = System::Ioutils::TPath::GetDocumentsPath() + PathDelim + cList[aIndex];
	 //
	 im -> Bitmap -> LoadFromFile(xName+".jpg");
	 //
	 TStringList *x = new TStringList;
	 __try {
		 x->LoadFromFile(xName+".txt");
		 tx->Text = x->Text;
	 }
	 __finally
	 {
		 x->DisposeOf();
	 }
 }
 //--------------------------------------------------------------------------

 //--------------------------------------------------------------------------

void __fastcall Tfm::buBackClick(TObject *Sender)
{
    tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::lbItemItemClick(TCustomListBox * const Sender, TListBoxItem * const Item)

{
     sbItem->ViewportPosition = TPointF(0,0);
	 LoadItem(Item->Index);
	 tx->RecalcSize();
	 tc->ActiveTab = tiItem;
}
//---------------------------------------------------------------------------

