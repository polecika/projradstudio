//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void LoadResourceToImage(UnicodeString aResName, TBitmap* aResult)
{
	TResourceStream* x;
	x = new TResourceStream((int)HInstance, aResName, RT_RCDATA);
	try {
	  aResult->LoadFromStream(x);
	}
	__finally {
		x->Free();
	}
}
//---------------------------------------------------------------------------
UnicodeString LoadResourceToText (UnicodeString aResName) {
	TResourceStream* x;
	TStringStream* xSS;
	x = new TResourceStream((int)HInstance, aResName, RT_RCDATA);
	try {
		//xSS = new TStringStream("", TEncoding::UTF8, true);
		xSS = new TStringStream("", TEncoding::UTF8, true);
		try {
					 xSS->LoadFromStream(x);
					 return xSS->DataString;
		}
		__finally
		 {
		 xSS->Free();
		}
	}
	__finally
	{
		x->Free();
	}
}

void __fastcall Tfm::CoupeClick(TObject *Sender)
{
	LoadResourceToImage("JpgImage_1", im->Bitmap);
	me->Text = LoadResourceToText("Resource_1");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::HardtopClick(TObject *Sender)
{
	LoadResourceToImage("JpgImage_2", im->Bitmap);
	me->Text = LoadResourceToText("Resource_1");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::CleanClick(TObject *Sender)
{
	me->Lines->Clear();
	im->Bitmap->SetSize(0,0);
}
//---------------------------------------------------------------------------
